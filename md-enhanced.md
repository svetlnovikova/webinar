---
description:
tags:
---
    
# Title

Some text

## Heading 2 

<!-- Note to self -->

## More Heading 2

Напишем тут текст. Больше текста. Еще больше текста.

### Heading 3

**bold**  
*italic*  
> Blockquote
>> Nested blockquote

```javascript {.line-numbers}
function add(x, y) {
  return x + y
}
```

@import "test.py" {class="line-numbers" line_begin=4 line_end=8}

## Links 

[Ссылка с подписью](https://www.google.es "This is google")

[Ссылка внутренняя][Google_URL] 

## Images

![картинка 1](/img/docs1.jpeg "Can't have Docs")  
![картинка 2](https://pbs.twimg.com/media/DwK1h3nX4AEECwH.jpg)
![картинка 3][IMG_REF_1]

### Horizontal rule:

---

## Code

This is one `inline code created using` backtick

``` bash
code block 
```

    notinline;
    print("hello, world!");
    z=x+y;

<!-- Refs -->

[IMG_REF_1]: /img/docs2.jpg "Do you read the docs"
[Google_URL]: google.com

<!-- Refs -->

> Blockquote

| header 1 | header 2 |
|----------|----------|
| item 1   | item 2   |
| ^  | item 2   |

Syntax highlighting

``` js
var foo = function (bar) {
  return bar++;
};

console.log(foo(5));
```

| Option | Description                                                               |
|--------|---------------------------------------------------------------------------|
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default.    |
| ext    | extension to be used for dest files.                                      |
> Blockquote

# Checkbox

- [ ] Not Done
- [x] Done
- [ ] Not Done

# Emoji
:smile:

## Mermaid

```mermaid
graph TD
A[Client] --> B[Load Balancer]
B --> C[Server01]
B --> D[Server02]
```

## PlantUML

```puml
A->B
B-->A
```

## Sequence

```sequence
Andrew->Xena: Says Hello
Note right of Xena: Xena thinks\nabout it
Xena-->Andrew: How are you?
Andrew->>Xena: I am good thanks!
```

[^1]: This is a footnote
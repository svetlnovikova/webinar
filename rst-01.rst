Heading 1
=========

.. а вот так пишутся комментарии.

*italic*
**bold**
``*``

Heading 2
^^^^^^^^^

#. Делай раз.
#. Делай два. 

* Так
* или вот так 

#. Можно делать список

   * внутри списка

   * прям внутри

#. И потом продолжать список.

Heading 1.2

Heading 3
"""""""""

#. А еще внутри списка могут быть ``codeblocks`` 

   .. code:: 

     "FEATURES": {
         ...
         'CERTIFICATES_HTML_VIEW': true,
         ...
     }

#. Что значит блоки кода.

.. note::

   Внимание, Внимание

.. warning::

    Предупреждаем

.. _Heading 3:

.. image:: img/docs2.jpg
   :width: 200
   :alt: good-doc

.. list-table::
    :widths: 25 25 50
    :header-rows: 1

 * - Строка 1
   - \-
   - Текст строки

 * - Строка 2
   - +
   - Текст строки

.. code::

         <problem>
             <annotationresponse>
                 <annotationinput>
                   <text>PLACEHOLDER: Text of annotation</text>
                     <comment>PLACEHOLDER: Text of question</comment>
                     <comment_prompt>PLACEHOLDER: Type your response below:</comment_prompt>
                     <tag_prompt>PLACEHOLDER: In your response to this question, which tag below
                     do you choose?</tag_prompt>
                   <options>
                     <option choice="incorrect">PLACEHOLDER: Incorrect answer (to make this
                     option a correct or partially correct answer, change choice="incorrect"
                     to choice="correct" or choice="partially-correct")</option>
                     <option choice="correct">PLACEHOLDER: Correct answer (to make this option
                     an incorrect or partially correct answer, change choice="correct" to
                     choice="incorrect" or choice="partially-correct")</option>
                     <option choice="partially-correct">PLACEHOLDER: Partially correct answer
                     (to make this option a correct or partially correct answer,
                     change choice="partially-correct" to choice="correct" or choice="incorrect")
                     </option>
                   </options>
                 </annotationinput>
             </annotationresponse>
             <solution>
               <p>PLACEHOLDER: Detailed explanation of solution</p>
             </solution>
           </problem